tmp = cv.FileStorage('C:\Users\Jason\Desktop\homographies.yml');
H = tmp.H;
H1 = zeros(2,length(H));
for ii = 1:length(H)
    H1(:,ii) = H{ii}(1:2,3);
end

H = zeros(21,21,2);
count = 1;
for ii = 1:21
    for jj = 1:21
        H(ii,jj,:) = H1(:,count);
        count = count+1;
    end
end

clear tmp ii jj count H1

% map image shifts to aperture shifts
tmp = squeeze(cat(2,H(10,11,:),H(11,10,:),H(11,12,:),H(12,11,:)));
tmp = max(abs(tmp),[],2);
tmp = 11/mean(tmp);
H = H*tmp;

H = reshape(H,[],2);

[xx,yy] = meshgrid(11*(-10:10),11*(-10:10));

oracle = [xx(:) yy(:)];

plot(H(:,1),H(:,2),'o'); hold on
plot(oracle(:,1),oracle(:,2),'+') 
axis square 