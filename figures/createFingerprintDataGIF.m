load('..\..\macro Phase Retrieval Large Data\f32_data\f32_fingerprint_jason_2\closed_aperture\f32_fingerprint_jason_2_21x21_81percent_overlap_20150813.mat')
ims = flip(ims,4);
%%
centerIm = squeeze(ims(:,:,11,11,:));
edgeIm = squeeze(ims(:,:,end,11,:));
%%
mosaicIm = mosaicImages(ims);
mosaicIm = imresize(mosaicIm,.1);
%%
tmp = mosaicIm;
for ii = 0:7
    tmp(:,:,ii+1) = cv.putText(tmp(:,:,ii+1),sprintf('Exp = %03dms',2^ii),[10 50],'Color',[255 255 255],'Thickness',2,'LineType','AA');
end